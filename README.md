# Bitbucket Pipelines Pipe: PHP Security Checker

This pipe will simply run [Composer audit](https://getcomposer.org/doc/03-cli.md#audit) on your project.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: swisnl/php-security-checker-pipe:2
```

## Variables

None.

## Prerequisites

When applicable, make sure [authentication for privately hosted packages and repositories](https://getcomposer.org/doc/articles/authentication-for-private-packages.md) is set up before running this pipe.

N.B. Authentication using SSH-keys is currently not supported.

## Examples

Basic example:

```yaml
script:
  - pipe: swisnl/php-security-checker-pipe:2
```

## Support

If you�d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by info@swis.nl.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce
